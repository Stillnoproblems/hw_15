﻿#include <iostream>
#include "PrintNumbers.h"

using namespace std;

int main()
{
	int N;
	
	cout << "Input N: ";
	cin >> N;
	cout << endl;
	
	for (int i = 0; i < N;)
	{
		if (i % 2 == 0)
		{
			cout << i << endl;
			i += 2;
			
		}
		else
		{
			i++;
		}
	}

	cout << endl << endl;
 
	cout << "Input 0 if you want to print only even numbers, else - 1" << endl;

	bool Condition;
	cin >> Condition;
	cout << endl;
	
	PrintNumbers(Condition, N);






}


